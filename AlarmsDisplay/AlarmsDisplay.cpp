// ========================================================
//  @author Joao Cosme:     joaorcosme@gmail.com
//  @author Ludmila Helena: ludmilahcunha@gmail.com

//  Universidade Federal de Minas Gerais
//  TP Automacao em Tempo Real - Junho/2017 
// ========================================================


#include "..\UtilsLib\Utils.h"

#include <windows.h>

#include <assert.h>
#include <iostream>
#include <sstream>
#include <stdlib.h>

// returns the alarm message in the following format:
// HH:MM:SS NSEQ: ###### TAG: ###### LINHA: # TEXTO: AAAAAA...AAAAAA
std::string const buildConsoleAlarmMessage(std::string const str) {
    std::vector<std::string> tokens;
    utils::tokenike(str, tokens, '|');
    assert(tokens.size() == 6);
     
    std::stringstream ss;
    ss << tokens[5]  // timestamp
       << " NSEQ: "  << tokens[0]
       << " TAG: "   << tokens[1]
       << " LINHA: " << tokens[3]
       << " TEXTO: " << tokens[4];

    return ss.str();
}

// receives alarm messages from a client process to display them in the console
// execution is synchronized with keyboard events from the parent process
int main(int argc, char ** argv) {
    
    HWND console = GetConsoleWindow();
    MoveWindow(console, 600, 0, 700, 350, TRUE);

    // mailslot server
    LPTSTR slot = TEXT(SLOT_NAME);
    HANDLE h_mailSlot = 
        CreateMailslot(slot, 0 /*maxMsgSize*/, 0 /* non-blocking */,
                        (LPSECURITY_ATTRIBUTES)NULL);
    if (h_mailSlot == INVALID_HANDLE_VALUE) {
        utils::printError("CreateMailSlot()");
        exit(1);
    }

    // open keyboard related events from the parent process
    HANDLE ev_toggle =
        OpenEvent(SYNCHRONIZE, FALSE, "ToggleDisplayAlarms");
    if (ev_toggle == NULL) {
        utils::printError("OpenEvent() [ToggleDisplayAlarms]");
    }
    HANDLE ev_clearAlarms =
        OpenEvent(SYNCHRONIZE, FALSE, "ClearAlarms");
    if (ev_clearAlarms == NULL) {
        utils::printError("OpenEvent() [ClearAlarms]");
    }
    HANDLE ev_escape =
        OpenEvent(SYNCHRONIZE, FALSE, "Escape");
    if (ev_escape == NULL) {
        utils::printError("OpenEvent() [Escape]");
    }

    bool hasValidHandles = ev_toggle && ev_escape && ev_clearAlarms;
    bool shutdown        = false;
    bool execFlag        = true;

    HANDLE keyboardEvents[3] = { ev_escape, ev_toggle, ev_clearAlarms };
    std::vector<std::string> retMessages;

    // iterativelly check if one the events was signaled
    while (!shutdown && hasValidHandles) {

        // using a 20ms timeout to avoid blocking the reading operation
        int status = WaitForMultipleObjects(3, keyboardEvents, FALSE, 20);
         
        if (status == WAIT_FAILED) {
            utils::printError("WaitForMultipleObjects()");
            shutdown = true;
        }
        else if ((status - WAIT_OBJECT_0) == 0) { // shutdown
            shutdown = true;
        }
        else if ((status - WAIT_OBJECT_0) == 1) { // toggle display
            execFlag = !execFlag;
        }
        else if ((status - WAIT_OBJECT_0) == 2) { // clear screen
            system("cls");
        }

        if (!shutdown && execFlag) {
            // read messages available in the mailsot
            status = utils::readSlot(h_mailSlot, retMessages);
            if (!status) {
                utils::printError("Failed to read mailslot");
                shutdown = true;
            }
            // display alarms
            for (const auto& msg : retMessages) {
                std::cout << buildConsoleAlarmMessage(msg) << std::endl;
            }

            retMessages.clear();
        }
    }
    
    CloseHandle(h_mailSlot);
    CloseHandle(ev_toggle);
    CloseHandle(ev_escape);
    CloseHandle(ev_clearAlarms);
}