// ========================================================
//  @author Joao Cosme:     joaorcosme@gmail.com
//  @author Ludmila Helena: ludmilahcunha@gmail.com

//  Universidade Federal de Minas Gerais
//  TP Automacao em Tempo Real - Junho/2017 
// ========================================================


#ifndef _CIRCULAR_BUFFER_H_
#define _CIRCULAR_BUFFER_H_

#include "..\UtilsLib\Utils.h"

#include <deque>
#include <memory>

class PLCMessage;

// a wrapper for a double-ended queue (aka std::deque)
class CircularBuffer {
private:
    CircularBuffer();

public:
    CircularBuffer(const size_t maxSize) : MAX_BUFFER_SIZE(maxSize) {}

    ~CircularBuffer() {}

    bool insertMessage(std::shared_ptr<PLCMessage> msgPtr,
                       bool overwrite = true);
    std::shared_ptr<PLCMessage> removeMessage(MessageType const type);

    size_t size() const;
    bool isFull() const;

private:
    std::deque<std::shared_ptr<PLCMessage>> dataBuffer;
    std::deque<std::shared_ptr<PLCMessage>> alarmBuffer;
    const size_t MAX_BUFFER_SIZE;
};

#endif