// ========================================================
//  @author Joao Cosme:     joaorcosme@gmail.com
//  @author Ludmila Helena: ludmilahcunha@gmail.com

//  Universidade Federal de Minas Gerais
//  TP Automacao em Tempo Real - Junho/2017 
// ========================================================


#include "MessageBuilder.h"
#include "PLCMessage.h"

#include <ctime>
#include <iomanip>
#include <memory>
#include <sstream>
#include <stdlib.h>

#define MAX_SEQ_NUM 1000000
#define MAX_DATA_VALUE 10000

namespace {
    // a floating-point value between 0 and MAX_DATA_VALUE
    double getRandomData() {
        return MAX_DATA_VALUE * static_cast<double>(rand()) / RAND_MAX;
    }

    // any upper-case letter {'A', 'B', 'C' ... 'Y'}
    char getRandomUpperCaseLetter() {
        return static_cast<char>(rand() % 25 + 65);
    }

    // build TAG in format "ABC-123"
    std::string buildRandomTAG() {
        std::stringstream ss;
        ss << getRandomUpperCaseLetter()
            << getRandomUpperCaseLetter()
            << getRandomUpperCaseLetter() << '-'
            << std::setw(3) << std::setfill('0') // zero padding
            << rand() % 1000;
        return ss.str();
    }

} // namespace

MessageBuilder::MessageBuilder(int plcNumber) : m_countSeqNum(1),
m_plcNum(plcNumber) {
    srand(static_cast<int>(time(NULL))); // seed to rand() 

    alarmMessages = { "DESALINHAMENTO CORREIA AREA 01",
                      "VEL CORREIA 01 ACIMA LIM MAX",
                      "VEL CORREIA 01 ABAIXO LIM MIN",
                      "DESALINHAMENTO CORREIA AREA 02",
                      "VEL CORREIA 02 ACIMA LIM MAX",
                      "VEL CORREIA 02 ABAIXO LIM MIN",
                      "NIVEL MAX SILO BRITADOR PRIM",
                      "BRITADOR PRIM OPERANDO A VAZIO",
                      "OBSTRUCAO CONE BRITADOR PRIM",
                      "SOBRECARGA NO MOTOR CORREIA 01",
                      "SOBRECARGA NO MOTOR CORREIA 02",
                      "SOBRECARGA MOTOR BRITADOR PRIM" };

    for (size_t i = 0; i < alarmMessages.size(); ++i) {
        alarmTags.push_back(buildRandomTAG());
    }
    assert(alarmMessages.size() == alarmTags.size());
}

int
MessageBuilder::getPLCNum() const {
    return m_plcNum;
}

std::shared_ptr<PLCMessage>
MessageBuilder::getNextMessage() {
    time_t rawtime;
    time(&rawtime);
    std::shared_ptr<PLCMessage> retMsgPtr;

    if (rand() % 2 == 1) {
        // get random alarm text and tag from vectors
        int idx = rand() % alarmTags.size();

        AlarmMessage* alarmMsgPtr =
            new AlarmMessage(m_countSeqNum, alarmTags[idx], rawtime, m_plcNum);
        alarmMsgPtr->setTextField(alarmMessages[idx]);
        retMsgPtr = std::shared_ptr<PLCMessage>(alarmMsgPtr);
    }
    else {
        DataMessage* dataMsgPtr =
            new DataMessage(m_countSeqNum, buildRandomTAG(), rawtime, m_plcNum);

        dataMsgPtr->setValueField(getRandomData());
        retMsgPtr = std::shared_ptr<PLCMessage>(dataMsgPtr);
    }

    // cyclic counting: 0 to 999999, 0 to 999999, ...
    ++m_countSeqNum;
    m_countSeqNum = m_countSeqNum == MAX_SEQ_NUM ? 0 : m_countSeqNum;

    return retMsgPtr;
}