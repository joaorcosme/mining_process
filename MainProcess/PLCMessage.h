// ========================================================
//  @author Joao Cosme:     joaorcosme@gmail.com
//  @author Ludmila Helena: ludmilahcunha@gmail.com

//  Universidade Federal de Minas Gerais
//  TP Automacao em Tempo Real - Junho/2017 
// ========================================================


#ifndef _PLC_MESSAGE_H_
#define _PLC_MESSAGE_H_

#include <assert.h>
#include <ctime>                
#include <string>


// base class to represent a PLC message
// it allows the messages to be easily generated and transformed into strings
// it will have 2 children: AlarmMessage and DataMessage
class PLCMessage {
public:
    PLCMessage(int seqNum, std::string tag, time_t timestamp, int plcNum) :
        m_seqNum(seqNum),
        m_tag(tag),
        m_rawtime(timestamp),
        m_plcNum(plcNum) {
        assert(tag.size() == 7);
        assert(plcNum == 1 || plcNum == 2);
    }

    PLCMessage() {};
    ~PLCMessage() {}

    int getPLCNumber() const;
    std::string getTag() const;
    int getSequenceNumber() const;
    time_t getTimestamp() const;
    std::string getTimestampStr() const;

    virtual std::string getMessageType() const { return 0; }
    virtual std::string toString() const { return "T"; }

private:
    int m_seqNum;
    int m_plcNum;
    std::string m_tag;
    time_t m_rawtime;
};

class AlarmMessage : public PLCMessage {
public:
    AlarmMessage(int seqNum, std::string tag, time_t timestamp, int plcNum) :
        PLCMessage(seqNum, tag, timestamp, plcNum) {
    }

    std::string getTextField() const;
    void setTextField(std::string text);

    virtual std::string getMessageType() const;
    virtual std::string toString() const;

private:
    std::string m_text;
};

class DataMessage : public PLCMessage {
public:
    DataMessage(int seqNum, std::string tag, time_t timestamp, int plcNum) :
        PLCMessage(seqNum, tag, timestamp, plcNum) {
    }

    double getValueField() const;
    void setValueField(double value);

    virtual std::string getMessageType() const;
    virtual std::string toString() const;

private:
    double m_value;
};

#endif