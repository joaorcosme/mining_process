// ========================================================
//  @author Joao Cosme:     joaorcosme@gmail.com
//  @author Ludmila Helena: ludmilahcunha@gmail.com

//  Universidade Federal de Minas Gerais
//  TP Automacao em Tempo Real - Junho/2017 
// ========================================================

#include <windows.h>

#include "CircularBuffer.h"
#include "MessageBuilder.h"
#include "PLCMessage.h"
#include "..\UtilsLib\Utils.h"

#include <conio.h>                                                           
#include <iostream>
#include <memory>
#include <sstream>

#define HAVE_STRUCT_TIMESPEC
#include <pthread.h>

// MAX number of messages
#define MAX_BUFFER_SIZE 200 
#define MAX_FILE_SIZE 100

#define ALARMS_EXE "..\\Debug\\AlarmsDisplay.exe"
#define DATA_EXE   "..\\Debug\\DataDisplay.exe"


// Global scope
// ============================================

CircularBuffer circularBuffer(MAX_BUFFER_SIZE);
pthread_mutex_t bufferGuard;

HANDLE h_timerQueue;

// Event Handles
HANDLE ev_readPLC1, ev_readPLC2,
    ev_readAlarms, ev_readData,
    ev_displayAlarms, ev_displayData,
    ev_clearAlarms, ev_escape,
    ev_bufferFull, ev_bufferFree,
    h_File, mtx_lockFile;

// ============================================



// this is a callback function called periodically by a TimerQueueTimer
// it insert PLC messages in the circular buffer 
VOID CALLBACK timerRoutinePLC(PVOID builder, BOOLEAN TimerOrWaitFired) {
    assert(builder);
    MessageBuilder* builderPLC = static_cast<MessageBuilder*>(builder);

    try { // any error in a Pthread call will throw an exception
        using namespace utils;

        int status = pthread_mutex_lock(&bufferGuard); // get access to the buffer
        if (status != 0) throw buildPthreadError("pthread_mutex_lock()", status);

        std::shared_ptr<PLCMessage> message = builderPLC->getNextMessage();
        assert(message);
   
        bool success = circularBuffer.insertMessage(message, false);

        if (circularBuffer.isFull()) {
            std::cout << "[WARN] Buffer cheio!\n" << std::endl;
            PulseEvent(ev_bufferFull);
        }

        status = pthread_mutex_unlock(&bufferGuard); // release access to the buffer
        if (status != 0) throw buildPthreadError("pthread_mutex_unlock()", status);

    }
    catch (const std::string error) {
        std::cout << error << std::endl;
        return;
    }
}



namespace plctimer {

    // insert hTimerP in the TimerQueue
    void start(DWORD period, PHANDLE hTimerQ, PHANDLE hTimerP,
               MessageBuilder* mBuilder) {
        assert(hTimerP && hTimerQ && mBuilder);

        if (!CreateTimerQueueTimer(hTimerP, *hTimerQ,
                                  (WAITORTIMERCALLBACK)timerRoutinePLC,
                                  (PVOID)mBuilder, period, period,
                                   WT_EXECUTEDEFAULT)) {
            utils::printError("CreateTimerQueueTimer()");
        }
    }

    // remove hTimerP from the TimerQueue
    void stop(PHANDLE hTimerQ, PHANDLE hTimerP) {
        assert(hTimerP && hTimerQ);

        if (!DeleteTimerQueueTimer(*hTimerQ, *hTimerP, NULL)) {
            if (GetLastError() == ERROR_IO_PENDING) {} // ok, IO was running
            else utils::printError("DeleteTimerQueueTimer()");
        }
    }

} // namespace plctimer



namespace utils {

    void informFreeBuffer() {
        std::cout << "[INFO] Buffer com posicao livre\n" << std::endl;
        PulseEvent(ev_bufferFree);
    }

    // check if there is a free position in the file (max = 100 msgs)
    int isFileFull(HANDLE& hFile) {
        DWORD size = GetFileSize(hFile, NULL);
        assert(size != INVALID_FILE_SIZE && (size % 38) == 0 );
        const DWORD MAX_BYTES = 38 * MAX_FILE_SIZE;           

        return size >= MAX_BYTES;
    }

    // print what key the user has pressed
    void printKeyStrokeMessage(std::string const msg, const char key, const bool stateFlag) {
        std::cout << "[INFO] Tecla '" << key << "' pressionada:\n       "
            << (stateFlag ? "Retomando " : "Suspendendo ")
            << msg << "\n" << std::endl;
    }

} // namespace utils



namespace tasks {

    // task to read messages from the PLCs (1 and 2)
    void * readPLC(void * arg) {
        assert(arg);

        int * plcNumber = static_cast<int*>(arg);
        assert(*plcNumber == 1 || *plcNumber == 2);

        DWORD period = *plcNumber * 1000; // timer period (ms)
        HANDLE keyboardEvents[3] =
        { ev_escape, *plcNumber == 1 ? ev_readPLC1 : ev_readPLC2, ev_bufferFull};

        HANDLE h_TimerPLC;
        MessageBuilder* builderPLC = new MessageBuilder(*plcNumber);
        plctimer::start(period, &h_timerQueue, &h_TimerPLC, builderPLC);

        bool shutdown = false, run = true;
        while (!shutdown) {
            int status = WaitForMultipleObjects(3, keyboardEvents, FALSE, INFINITE);

            if (run) {
                plctimer::stop(&h_timerQueue, &h_TimerPLC);
            }

            if (status == WAIT_FAILED) {
                utils::printError("WaitForMultipleObjects() [@readPLC thread]");
                shutdown = true;
            }
            else {
                switch (status - WAIT_OBJECT_0) {
                case 0: // user pressed Esc
                    shutdown = true;
                    break;
                case 1: // user pressed key to pause/resume reading a PLC
                    if (!run) {
                        plctimer::start(period, &h_timerQueue, &h_TimerPLC, builderPLC);
                    }
                    run = !run;
                    break;
                case 2: // buffer is full

                    // since we will be waiting for the buffer to be free, it's
                    // also necessary to check the Esc event here
                    HANDLE expectedEvents[2] = { ev_escape, ev_bufferFree };
                    status = WaitForMultipleObjects(2, expectedEvents, FALSE, INFINITE);

                    if (status == WAIT_FAILED) {
                        utils::printError("WaitForMultipleObjects() [@readPLC thread]");
                        shutdown = true;
                    }
                    else if ((status - WAIT_OBJECT_0) == 0) { // ESC
                        shutdown = true;
                    }
                    else if ((status - WAIT_OBJECT_0) == 1) { // buffer is free
                        if (run) {
                            plctimer::start(period, &h_timerQueue, &h_TimerPLC, builderPLC);
                        }
                    }
                    break;
                }
            }            
        }

        delete builderPLC;
        return nullptr;
    }


    // task to read alarms from the circular buffer
    void * readAlarmsFromBuffer(void * args) {
        Sleep(2000); // time to allow mailslot server to be initialized

        HANDLE hFile = CreateFile(SLOT_NAME, GENERIC_WRITE, FILE_SHARE_READ,
            (LPSECURITY_ATTRIBUTES)NULL, OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL, (HANDLE)NULL);
        if (hFile == INVALID_HANDLE_VALUE) {
            utils::printError("CreateFile() [@readAlarmsFromBuffer thread]");
            return nullptr;
        }

        HANDLE keyboardEvents[2] = { ev_escape, ev_readAlarms };
        bool shutdown = false, run = true;
        DWORD waitTime = 20; // miliseconds

        while (!shutdown) {
            int status = WaitForMultipleObjects(2, keyboardEvents, FALSE, waitTime);

            if (status == WAIT_FAILED) {
                utils::printError( "WaitForMultipleObjects() [@readAlarmsFromBuffer thread]");
                shutdown = true;
            }
            else {
                switch (status - WAIT_OBJECT_0) { // which event triggered?
                case 0: // user pressed Esc
                    shutdown = true;
                    break;
                case 1: // user pressed key to pause/resume reading alarms from buffer
                    run = !run;
                    waitTime = run ? 20 : INFINITE;
                    break;
                }
            }

            if (run) {
                
                try { // any error in a Pthread call will throw an exception
                    using namespace utils;

                    // 1 --- Get access to the circular buffer
                    status = pthread_mutex_lock(&bufferGuard);
                    if (status != 0) throw buildPthreadError("pthread_mutex_lock()", status);

                    // 2 --- Remove alarm msg from buffer
                    const bool wasFull = circularBuffer.isFull();
                    std::shared_ptr<PLCMessage> msg = circularBuffer.removeMessage(ALARM);

                    if (wasFull && !circularBuffer.isFull()) { // there is a free position now
                        utils::informFreeBuffer();
                    }

                    // 3 --- Release access to the circular buffer
                    status = pthread_mutex_unlock(&bufferGuard);
                    if (status != 0) throw buildPthreadError("pthread_mutex_unlock()", status);

                    if (msg) { // send alarm msg through mailslot
                        utils::writeSlot(hFile, msg->toString());
                    }

                }
                catch (const std::string error) {
                    std::cout << error << std::endl;
                    shutdown = true;
                }
            }         
        }

        CloseHandle(hFile);
        return nullptr;
    }


    // task to read data from the circular buffer
    void * readDataFromBuffer(void * args) {

        CircularBuffer bufferToFile(MAX_FILE_SIZE);

        HANDLE keyboardEvents[2] = { ev_escape, ev_readData };
        DWORD waitTime = 20; // miliseconds
        bool shutdown = false, run = true, fileFlag = false;

        while (!shutdown) {
            int status = WaitForMultipleObjects(2, keyboardEvents, FALSE, waitTime);

            if (status == WAIT_FAILED) {
                utils::printError( "WaitForMultipleObjects() [@readDataFromBuffer thread]");
                shutdown = true;
            }
            else {
                switch (status - WAIT_OBJECT_0) { // which event triggered?
                case 0: // user pressed Esc
                    shutdown = true;
                    break;
                case 1: // user pressed key to pause/resume reading data from buffer
                    run = !run;
                    waitTime = run ? 20 : INFINITE;
                    break;
                }
            }

            if (run) {        
                status = WaitForSingleObject(mtx_lockFile, 1); // get acces to file

                if (status == WAIT_FAILED) {
                    utils::printError( "WaitForSingleObject()"
                        "[@readDataFromBuffer thread]");
                    shutdown = true;
                }
                else if (status == WAIT_OBJECT_0) {
                    if (!utils::isFileFull(h_File)) {
                        fileFlag = false;
                        waitTime = 20;

                        try { // any error in a Pthread call will throw an exception
                            using namespace utils;

                            // 1 --- Get access to the circular buffer
                            status = pthread_mutex_lock(&bufferGuard);
                            if (status != 0)
                                throw buildPthreadError("pthread_mutex_lock()", status);

                            // 2 --- Remove data from buffer
                            const bool wasFull = circularBuffer.isFull();
                            std::shared_ptr<PLCMessage> msg = circularBuffer.removeMessage(DATA);

                            if (wasFull && !circularBuffer.isFull()) { // there is a free position
                                utils::informFreeBuffer();
                            }

                            // 3 --- Release access to the circular buffer
                            status = pthread_mutex_unlock(&bufferGuard);
                            if (status != 0)
                                throw buildPthreadError("pthread_mutex_unlock()", status);

                            if (msg) { // write data to file
                                std::string toWrite(msg->toString());
                                toWrite += "\r\n"; // append line break

                                SetFilePointer(h_File, 0, NULL, FILE_END);
                                utils::writeFile(h_File, toWrite);
                            } 

                        }
                        catch (const std::string error) {
                            std::cout << error << std::endl;
                            shutdown = true;
                        }   
                    }
                    else if (!fileFlag) {
                        std::cout << "[WARN] Arquivo cheio!\n       "
                                  << "Suspendendo retirada de dados de processo do buffer\n"
                                  << std::endl;
                        fileFlag = true;
                        waitTime = 500;
                    }

                    ReleaseMutex(mtx_lockFile); // release acces to file
                } 
            }         
        }

        return nullptr;
    }


    // task to monitor the mapped keys 
    void * keyboardMonitor(void * args) {

        bool key_u = false, key_d = false, key_a = false, key_p = false;

        while (true) {
            switch (_getch()) {
            case '1':   // pause/resume reading PLC1
                std::cout << "[INFO] Tecla 1 pressionada [leitura PLC1]\n" << std::endl;
                PulseEvent(ev_readPLC1);
                break;
            case '2':   // pause/resume reading PLC2
                std::cout << "[INFO] Tecla 2 pressionada [leitura PLC2]\n" << std::endl;      
                PulseEvent(ev_readPLC2);
                break;
            case 'u':   // pause/resume reading alarms from buffer
                utils::printKeyStrokeMessage("leitura de alarmes do buffer", 'u', key_u);
                key_u = !key_u;
                PulseEvent(ev_readAlarms);
                break;
            case 'd':   // pause/resume reading data from buffer
                utils::printKeyStrokeMessage("leitura de dados de processo do buffer", 'd', key_d);
                key_d = !key_d;
                PulseEvent(ev_readData);
                break;
            case 'a':   // pause/resume alarms display
                utils::printKeyStrokeMessage("exibicao de mensagens de alarme", 'a', key_a);
                key_a = !key_a;
                PulseEvent(ev_displayAlarms);
                break;
            case 'p':   // pause/resume data display
                utils::printKeyStrokeMessage("exibicao de dados de processo", 'p', key_p);
                key_p = !key_p;
                PulseEvent(ev_displayData);
                break;
            case 'c':   // clear alarms display
                std::cout << "[INFO] Tecla 'c' pressionada:\n       "
                    << "Limpando a tela de alarmes\n" << std::endl;
                PulseEvent(ev_clearAlarms);
                break;
            case 27:    // ESC: shutdown all tasks
                std::cout << "[INFO] Tecla 'Esc' pressionada:\n       "
                    << "Encerrando todas as tarefas\n" << std::endl;
                PulseEvent(ev_escape);
                return nullptr;
            case 'b':
                try { // any error in a Pthread call will throw an exception
                    using namespace utils;

                    int status = pthread_mutex_lock(&bufferGuard);
                    if (status != 0) throw buildPthreadError("pthread_mutex_lock", status);
                    std::cout << "[DEBUG] Mensagens no buffer: " << circularBuffer.size()
                              << "\n" << std::endl;
                    status = pthread_mutex_unlock(&bufferGuard);
                    if (status != 0) throw buildPthreadError("pthread_mutex_unlock", status);

                    break;
                }
                catch (const std::string error) {
                    std::cout << error << std::endl;
                    return nullptr;
                }                
            }
        }

        return nullptr;
    }
}

namespace winapiwrapper {

    HANDLE createEvent(char * eventName) {
        assert(eventName);
        LPCSTR name = TEXT(eventName);
        HANDLE retEvent = CreateEvent(NULL, TRUE /*manual-reset*/, FALSE, name);
        if (retEvent == NULL) {
            std::stringstream ss;
            ss << "CreateEvent() [" << eventName << "]";
            utils::printError(ss.str());
        }
        return retEvent;
    }

    // spawns child process in a new console
    bool spawnProcess(char const * processName, char * title, DWORD color) {
        assert(processName && title);

        STARTUPINFO si;
        PROCESS_INFORMATION pi;
        SecureZeroMemory(&si, sizeof(si));
        SecureZeroMemory(&pi, sizeof(pi));

        si.lpTitle = title;
        si.dwFlags = STARTF_USEFILLATTRIBUTE;
        si.dwFillAttribute = color | BACKGROUND_INTENSITY;
        si.cb = sizeof(si);

        bool status =
            CreateProcess(processName,
                NULL,   // command line
                NULL,   // process handle inheritance
                NULL,   // thread handle inheritance
                FALSE,  // handle inheritance
                CREATE_NEW_CONSOLE,
                NULL,   // use parent's environment block
                NULL,   // use parent's starting directory 
                &si, &pi);

        if (!status) {
            std::stringstream ss;
            ss << "CreateProcess() [" << title << "]";
            utils::printError(ss.str());
        }

        return status;
    }
} // namespace winapiwrap




int main(int argc, char** argv) {

    HWND console = GetConsoleWindow();
    MoveWindow(console, 0, 0, 600, 700, TRUE);
    SetConsoleTitle("TERMINAL DO USUARIO");
     
    ev_readPLC1      = winapiwrapper::createEvent("TogglePLC1");
    ev_readPLC2      = winapiwrapper::createEvent("TogglePLC2");
    ev_readAlarms    = winapiwrapper::createEvent("ToggleReadAlarms");
    ev_readData      = winapiwrapper::createEvent("ToggleReadData");
    ev_displayAlarms = winapiwrapper::createEvent("ToggleDisplayAlarms");
    ev_displayData   = winapiwrapper::createEvent("ToggleDisplayData");
    ev_clearAlarms   = winapiwrapper::createEvent("ClearAlarms");
    ev_escape        = winapiwrapper::createEvent("Escape");

    ev_bufferFull    = winapiwrapper::createEvent("BufferFull");
    ev_bufferFree    = winapiwrapper::createEvent("BufferFree");

    // create file to save data messages
    h_File = CreateFile(DATA_FILENAME, (GENERIC_WRITE | GENERIC_READ),
                (FILE_SHARE_READ | FILE_SHARE_WRITE),
                (LPSECURITY_ATTRIBUTES)NULL, CREATE_ALWAYS,
                FILE_ATTRIBUTE_NORMAL, (HANDLE)NULL);
    if (h_File == INVALID_HANDLE_VALUE) {
        utils::printError("CreateFile()");
    }

    // create mutex to lock/unlock file accessed by this process and a child process
    mtx_lockFile = CreateMutex(NULL, FALSE, "lockFile");
    if (mtx_lockFile == NULL) {
        utils::printError("CreateMutex()");
    }

    // create TimerQueue to generate PLC messages periodically
    h_timerQueue = CreateTimerQueue();
    if (h_timerQueue == NULL) {
        utils::printError("CreateTimerQueue()");
    }

    // move forward only if all handles are valid
    if (ev_readPLC1 && ev_readPLC2 && ev_readAlarms &&
        ev_readData && ev_displayAlarms && ev_displayData &&
        ev_clearAlarms && ev_escape && h_timerQueue && h_File && mtx_lockFile) {

        try { // any error in a Pthread call will throw an exception

            using namespace utils;

            // create mutex to lock/unlock the circular buffer
            int status = pthread_mutex_init(&bufferGuard, NULL);
            if (status != 0) throw buildPthreadError("pthread_mutex_init()", status);

            // create all threads
            const int PLC1 = 1;
            pthread_t readPLC1Thread;
            std::cout << "[INFO] Iniciando tarefa de leitura do PLC " << PLC1 << std::endl;
            status = pthread_create(&readPLC1Thread, NULL, tasks::readPLC, (void*)&PLC1);
            if (status != 0) throw buildPthreadError("pthread_mutex_create()", status);

            const int PLC2 = 2;
            pthread_t readPLC2Thread;
            std::cout << "[INFO] Iniciando tarefa de leitura do PLC " << PLC2 << std::endl;
            status = pthread_create(&readPLC2Thread, NULL, tasks::readPLC, (void *)&PLC2);
            if (status != 0) throw buildPthreadError("pthread_mutex_create()", status);

            pthread_t readAlarmsThread;
            std::cout << "[INFO] Iniciando tarefa de leitura de alarmes no buffer" << std::endl;
            status = pthread_create(&readAlarmsThread, NULL, tasks::readAlarmsFromBuffer, NULL);
            if (status != 0) throw buildPthreadError("pthread_mutex_create()", status);

            pthread_t readDataThread;
            std::cout << "[INFO] Iniciando tarefa de leitura de dados de processo no buffer"
                << std::endl;
            status = pthread_create(&readDataThread, NULL, tasks::readDataFromBuffer, NULL);
            if (status != 0) throw buildPthreadError("pthread_mutex_create()", status);

            pthread_t readKeyboardThread;
            std::cout << "[INFO] Iniciando tarefa de leitura do teclado" << std::endl;
            status = pthread_create(&readKeyboardThread, NULL, tasks::keyboardMonitor, NULL);
            if (status != 0) throw buildPthreadError("pthread_mutex_create()", status);


            bool processStatus =
                winapiwrapper::spawnProcess(ALARMS_EXE, "TERMINAL DE ALARMES", BACKGROUND_RED);
            std::cout << "[INFO] Iniciando tarefa de exibicao de alarmes" << std::endl;

            processStatus &=
                winapiwrapper::spawnProcess(DATA_EXE, "TERMINAL DE PROCESSO", BACKGROUND_GREEN);
            std::cout << "[INFO] Iniciando tarefa de exibicao de dados de processo\n" << std::endl;
            std::cout << "--------------------------------------------------------\n" << std::endl;


            Sleep(1000);
            SetFocus(console); // focus on the user terminal


            // wait all threads to finish
            if (processStatus) {
                pthread_join(readPLC1Thread, NULL);
                std::cout << "[INFO] Encerrando tarefa de leitura do PLC 1" << std::endl;

                pthread_join(readPLC2Thread, NULL);
                std::cout << "[INFO] Encerrando tarefa de leitura do PLC 2" << std::endl;

                pthread_join(readDataThread, NULL);
                std::cout << "[INFO] Encerrando tarefa de leitura de dados de processo no buffer"
                    << std::endl;

                pthread_join(readAlarmsThread, NULL);
                std::cout << "[INFO] Encerrando tarefa de leitura de alarmes no buffer"
                    << std::endl;

                pthread_join(readKeyboardThread, NULL);
                std::cout << "[INFO] Encerrando tarefa de leitura do teclado\n" << std::endl;
            }   

        }
        catch (const std::string error) {
            std::cout << error << std::endl;
        }             
       
    }


    std::cout << "[INFO] Encerrando tarefa principal      " << std::endl;
    std::cout << "       Pressione uma tecla para sair..." << std::endl;
    _getch();

    DeleteTimerQueueEx(h_timerQueue, INVALID_HANDLE_VALUE);

    CloseHandle(ev_readPLC1);
    CloseHandle(ev_readPLC2);
    CloseHandle(ev_readAlarms);
    CloseHandle(ev_readData);
    CloseHandle(ev_displayAlarms);
    CloseHandle(ev_displayData);
    CloseHandle(ev_clearAlarms);
    CloseHandle(ev_escape);
    CloseHandle(h_File);
    CloseHandle(mtx_lockFile);

    pthread_mutex_destroy(&bufferGuard);

    return 0;
}