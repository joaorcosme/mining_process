// ========================================================
//  @author Joao Cosme:     joaorcosme@gmail.com
//  @author Ludmila Helena: ludmilahcunha@gmail.com

//  Universidade Federal de Minas Gerais
//  TP Automacao em Tempo Real - Junho/2017 
// ========================================================


#include "CircularBuffer.h"
#include "PLCMessage.h"
#include "..\UtilsLib\Utils.h"

#include <assert.h>
#include <deque>
#include <string>
#include <memory>


bool
CircularBuffer::insertMessage(std::shared_ptr<PLCMessage> msgPtr, bool overwrite) {
    assert(msgPtr);
    std::string type = msgPtr->getMessageType();
    bool isAlarm = type.compare("11") == 0;
    
    if (isFull()) {
        if (!overwrite) {
            return false;
        }
        if (isAlarm) {
            alarmBuffer.pop_front();
        }
        else {
            dataBuffer.pop_front();
        }
    }

    if (isAlarm) {
        alarmBuffer.push_back(msgPtr);
    }
    else {
        dataBuffer.push_back(msgPtr);
    }
    return true;
}

std::shared_ptr<PLCMessage>
CircularBuffer::removeMessage(MessageType const type) {
    std::shared_ptr<PLCMessage> retMsg = nullptr;
    if (type == ALARM && !alarmBuffer.empty()) {
        retMsg = alarmBuffer.front();
        alarmBuffer.pop_front();
    }
    else if (type == DATA && !dataBuffer.empty()) {
        retMsg = dataBuffer.front();
        dataBuffer.pop_front();
    }
    return retMsg;
}

size_t
CircularBuffer::size() const {
    return alarmBuffer.size() + dataBuffer.size();
}

bool
CircularBuffer::isFull() const {
    return size() == MAX_BUFFER_SIZE;
}