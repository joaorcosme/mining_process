// ========================================================
//  @author Joao Cosme:     joaorcosme@gmail.com
//  @author Ludmila Helena: ludmilahcunha@gmail.com

//  Universidade Federal de Minas Gerais
//  TP Automacao em Tempo Real - Junho/2017 
// ========================================================


#include "PLCMessage.h"

#include <assert.h>
#include <ctime>
#include <iomanip>
#include <string>
#include <sstream>

// Class PLCMessage

int
PLCMessage::getPLCNumber() const {
    return m_plcNum;
}

std::string
PLCMessage::getTag() const {
    return m_tag;
}

int
PLCMessage::getSequenceNumber() const {
    return m_seqNum;
}

time_t
PLCMessage::getTimestamp() const {
    return m_rawtime;
}

std::string
PLCMessage::getTimestampStr() const {
    char buffer[16];
    struct tm timeinfo;
    time_t raw = getTimestamp();

    localtime_s(&timeinfo, &raw);
    strftime(buffer, 16, "%T", &timeinfo); // get format HH:MM:SS

    std::string timestamp(buffer);
    assert(timestamp.size() == 8);
    return timestamp;
}

// class AlarmMessage

std::string
AlarmMessage::getTextField() const {
    return m_text;
}

void
AlarmMessage::setTextField(std::string text) {
    m_text = text;
}

std::string
AlarmMessage::getMessageType() const {
    std::string type("11");
    return type;
}

std::string
AlarmMessage::toString() const {
    std::stringstream ss;
    ss << std::setw(6) << std::setfill('0')  // zero padding
        << getSequenceNumber() << '|'
        << getTag() << '|'
        << getMessageType() << '|'
        << getPLCNumber() << '|'
        << std::setw(30) << std::setfill(' ')
        << getTextField() << '|'
        << getTimestampStr();

    return ss.str();
}

// class DataMessage

double
DataMessage::getValueField() const {
    return m_value;
}

void
DataMessage::setValueField(double value) {
    m_value = value;
}

std::string
DataMessage::getMessageType() const {
    std::string type("00");
    return type;
}

std::string
DataMessage::toString() const {
    std::stringstream ss;
    ss << std::setw(6) << std::setfill('0')    // zero padding
        << getSequenceNumber() << '|'
        << getTag() << '|'
        << getMessageType() << '|'
        << getPLCNumber() << '|'
        << std::setw(7) << std::setfill('0')    // zero padding
        << std::fixed << std::setprecision(1) // digits in decimal part
        << getValueField() << '|'
        << getTimestampStr();

    return ss.str();
}