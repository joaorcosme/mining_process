// ========================================================
//  @author Joao Cosme:     joaorcosme@gmail.com
//  @author Ludmila Helena: ludmilahcunha@gmail.com

//  Universidade Federal de Minas Gerais
//  TP Automacao em Tempo Real - Junho/2017 
// ========================================================


#ifndef _MESSAGE_BUILDER_H_
#define _MESSAGE_BUILDER_H_

#include <memory>
#include <vector>

class PLCMessage;

// class to generate PLC messages
// when getNextMessage() is called, a new message is returned with the
// corresponding sequence number, timestamp and other information
class MessageBuilder
{
public:
    MessageBuilder(int plcNumber);
    ~MessageBuilder() {};
    std::shared_ptr<PLCMessage> getNextMessage();
    int getPLCNum() const;

private:
    int m_countSeqNum;
    int m_plcNum;
    std::vector<std::string> alarmMessages;
    std::vector<std::string> alarmTags;
};

#endif