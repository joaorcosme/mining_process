// ========================================================
//  @author Joao Cosme:     joaorcosme@gmail.com
//  @author Ludmila Helena: ludmilahcunha@gmail.com

//  Universidade Federal de Minas Gerais
//  TP Automacao em Tempo Real - Junho/2017 
// ========================================================


#include "..\UtilsLib\Utils.h"

#include <windows.h>

#include <assert.h>
#include <iostream>
#include <sstream>
#include <stdlib.h>

// returns the data message in the following format:
// HH:MM:SS NSEQ: ###### TAG: ###### LINHA: # VALOR: NNNNN.N
std::string const buildConsoleDataMessage(std::string const str) {
    std::vector<std::string> tokens;
    utils::tokenike(str, tokens, '|');
    assert(tokens.size() == 6);

    std::stringstream ss;
    ss << tokens[5]  // timestamp
        << " NSEQ: "  << tokens[0]
        << " TAG: "   << tokens[1]
        << " LINHA: " << tokens[3]
        << " VALOR: " << tokens[4];

    return ss.str();
}

// reads data messages from a file to display them in the console
// execution is synchronized with keyboard events from the parent process
int main(int argc, char ** argv) {

    HWND console = GetConsoleWindow();
    MoveWindow(console, 600, 350, 700, 350, TRUE);

    // Create file to read process data
    HANDLE h_File = CreateFile(DATA_FILENAME, (GENERIC_READ | GENERIC_WRITE),
            (FILE_SHARE_WRITE | FILE_SHARE_READ),
            (LPSECURITY_ATTRIBUTES)NULL, OPEN_ALWAYS,
            FILE_ATTRIBUTE_NORMAL, (HANDLE)NULL);
    if (h_File == INVALID_HANDLE_VALUE) {
        utils::printError("CreateFile()");
        exit(1);
    }

    // open keyboard related events from the parent process
    HANDLE ev_toggle =
        OpenEvent(SYNCHRONIZE, FALSE, "ToggleDisplayData");
    if (ev_toggle == NULL) {
        utils::printError("OpenEvent() [ToggleDisplayData]");
    }
    HANDLE ev_escape =
        OpenEvent(SYNCHRONIZE, FALSE, "Escape");
    if (ev_escape == NULL) {
        utils::printError("OpenEvent() [Escape]");
    }

    HANDLE mtx_lockFile = OpenMutex(SYNCHRONIZE, FALSE, "lockFile");
    if (mtx_lockFile == NULL) {
        utils::printError("CreateMutex()");
    }

    bool hasValidHandles = ev_toggle && ev_escape;
    bool shutdown        = false;
    bool execFlag        = true;

    HANDLE keyboardEvents[3] = { ev_escape, ev_toggle };
    std::string retMessage;

    // iterativelly check if one the events was signaled
    while (!shutdown && hasValidHandles) {

        // using a 20ms timeout to avoid blocking the reading operation
        int status = WaitForMultipleObjects(2, keyboardEvents, FALSE, 20);

        if (status == WAIT_FAILED) {
            utils::printError("WaitForMultipleObjects()");
            shutdown = true;
        }
        else if ((status - WAIT_OBJECT_0) == 0) { // shutdown
            shutdown = true;
        }
        else if ((status - WAIT_OBJECT_0) == 1) { // toggle display
            execFlag = !execFlag;
        }

        if (!shutdown && execFlag) {
           
           int status = WaitForSingleObject(mtx_lockFile, 20); // get access to file

           if (status == WAIT_FAILED) {
               utils::printError( "WaitForSingleObject()");
               shutdown = true;
           }
           else if (status == WAIT_OBJECT_0) {
               
               // read all lines and copy to vector
               std::vector<std::string> allMessages;
               bool read = utils::readAllLinesInFile(h_File, allMessages);
               if (!read) {
                   utils::printError("Failed to read file");
                   shutdown = true;
               }

               // set EOF to the beggining: messages read are removed from file
               status = SetFilePointer(h_File, 0, NULL, FILE_BEGIN); // move file pointer...
               if (status == INVALID_SET_FILE_POINTER) {
                   utils::printError("SetFilePointer()");
                   shutdown = true;
               }
               else {
                   bool status = SetEndOfFile(h_File); // ... and set to EOF
                   if (!status) {
                       utils::printError("SetEndOfFile()");
                       shutdown = true;
                   }
               }
      
               // print all messages we read to the console
               for (auto& msg : allMessages) {
                   utils::removeLineBreak(msg);
                   std::cout << buildConsoleDataMessage(msg) << std::endl;
               }
               allMessages.clear();

               ReleaseMutex(mtx_lockFile); // release access to file
           }
        }
    }

    CloseHandle(h_File);
    CloseHandle(ev_toggle);
    CloseHandle(ev_escape);
    CloseHandle(mtx_lockFile);
}