// ========================================================
//  @author Joao Cosme:     joaorcosme@gmail.com
//  @author Ludmila Helena: ludmilahcunha@gmail.com

//  Universidade Federal de Minas Gerais
//  TP Automacao em Tempo Real - Junho/2017 
// ========================================================

#include "Utils.h"

#include <windows.h>

#include <assert.h>  
#include <cstring>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

void
utils::printError(const std::string str) {
    std::cout << "[ERROR] \""
              << str << "\" - [code] "
              << GetLastError() << std::endl;
}

const std::string
utils::buildPthreadError(std::string str, const int en) {
    std::stringstream ss;
    ss  << "[ERROR - Pthreads] \""
        << str << "\" - [code] "
        << en << std::endl;
    return ss.str();
}

void
utils::removeLineBreak(std::string& str) {
    if(!str.empty() && (str[str.length() - 1] == '\n')) {
        str.erase(str.length() - 1, 1);
    }
    if(!str.empty() && (str[str.length() - 1] == '\r')) {
        str.erase(str.length() - 1, 1);
    }
}

void
utils::tokenike(std::string const message, std::vector<std::string>& retTokens, const char delim) {
    retTokens.clear();
    std::istringstream toParse(message);
    std::string token;

    while(getline(toParse, token, delim)) {
        removeLineBreak(token);
        retTokens.push_back(token);
    }
}

bool utils::writeSlot(HANDLE& hSlot, const std::string msg) {
    assert(!msg.empty());

    DWORD bytesToWrite = msg.size() + 1;
    DWORD bytesWritten = 0;

    bool status = WriteFile(hSlot, msg.c_str(), bytesToWrite, &bytesWritten, NULL);
    if (!status) {
        printError("WriteFile() -- Mailslot");
    }

    return status;
}

bool utils::readSlot(HANDLE& hSlot, std::vector<std::string>& msgs) {
    DWORD sizeNextMsg = 0, numberMsgs = 0;
    bool status = GetMailslotInfo(hSlot, NULL, &sizeNextMsg, &numberMsgs, NULL);
    if (!status) {
        printError("GetMailslotInfo()");
    }

    if (status && sizeNextMsg != MAILSLOT_NO_MESSAGE) {
        DWORD sizeRead = 0;
        char * buffer = new char[sizeNextMsg + 1];
        //for (size_t i = 0; i < numberMsgs; ++i) {
            status = ReadFile(hSlot, buffer, sizeNextMsg, &sizeRead, NULL);
            if (!status) {
                printError("ReadFile() -- Mailslot");
                msgs.clear();
                //break;
            }
            std::string msg(buffer);
            msgs.push_back(msg);
        //}
        delete[] buffer;
    }
    return status;
}

bool utils::writeFile(HANDLE& hFile, std::string msg) {
    assert(!msg.empty());

    DWORD bytesToWrite = msg.size();
    DWORD bytesWritten = 0;

    bool status = WriteFile(hFile, msg.c_str(), bytesToWrite, &bytesWritten, NULL);
    if (!status) {
        printError("WriteFile()");
    }

    return status;
}

// we assume each line is 39 bytes long
bool utils::readLineInFile(HANDLE& hFile, std::string& msg) {
    const size_t BUFFER_SIZE = 39; // 36 (msg) + "\n\r\0" = 39
    DWORD sizeRead = 0;
    char * buffer = new char[BUFFER_SIZE];

    bool status = ReadFile(hFile, buffer, BUFFER_SIZE - 1, &sizeRead, NULL);

    if (!status) {
        printError("ReadFile()");
    } 
    else {
        assert(sizeRead >= 0 && sizeRead < BUFFER_SIZE);
        buffer[sizeRead] = '\0';
        msg = buffer;
    }

    delete[] buffer;
    return status;
}

bool utils::readAllLinesInFile(HANDLE& hFile, std::vector<std::string>& msgs) {
    std::string msg;    
    do {
        msg.clear();
        bool status = readLineInFile(hFile, msg);
        if (!status) {
            return false;
        }
        if (!msg.empty()) {
            msgs.push_back(msg);
        }        
    }
    while (!msg.empty());
   
    return true;
}