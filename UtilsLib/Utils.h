// ========================================================
//  @author Joao Cosme:     joaorcosme@gmail.com
//  @author Ludmila Helena: ludmilahcunha@gmail.com

//  Universidade Federal de Minas Gerais
//  TP Automacao em Tempo Real - Junho/2017 
// ========================================================


#ifndef _UTILS_H_
#define _UTILS_H_

#include <windows.h>

#include <string>
#include <vector>

#define SLOT_NAME "\\\\.\\mailslot\\Alarms"
#define DATA_FILENAME  "..\\Debug\\DataFile"

enum MessageType {ALARM, DATA};


// set of useful methods
namespace utils {
    void printError(const std::string str);

    const std::string buildPthreadError(std::string str, const int en);

    void tokenike(const std::string str, std::vector<std::string>& retTokens, const char delim);

    void removeLineBreak(std::string& str);

    bool writeSlot(HANDLE& hSlot, const std::string msg);

    bool readSlot(HANDLE& hSlot, std::vector<std::string>& msgs);

    bool writeFile(HANDLE& hSlot, std::string msg);

    bool readLineInFile(HANDLE& hFile, std::string& msg);

    bool readAllLinesInFile(HANDLE& hFile, std::vector<std::string>& msg);

} // namespace utils

#endif
